
library(tidyverse)
library(here)
library(fs)

# Run inside the r-cubed RStudio R Project.
plot_quantitative_feedback <- here("feedback") %>%
    dir_ls(regexp = "20(20|21).*[0-9]-quantitative.*") %>%
    map_dfr(read_csv, show_col_types = FALSE) %>%
    mutate(response = fct_relevel(response, "Strongly agree", "Agree", "Neutral", "Disagree") %>%
               fct_rev(),
           survey_item = str_wrap(survey_item, 30)) %>%
    count(survey_item, response) %>%
    filter(str_detect(survey_item, "(website|alignment|good course)", negate = TRUE)) %>%
    ggplot(aes(y = response, x = n, label = n)) +
    geom_col(fill = "#008ab7") +
    geom_text(nudge_x = 3) +
    facet_wrap(~ survey_item) +
    theme_minimal() +
    theme(
        axis.title.y = element_blank(),
        axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.line.x = element_blank(),
        panel.grid = element_blank(),
        plot.title = element_text(hjust = 0.5)
    ) +
    labs(title = "Quantitative feedback from participants\non how they viewed the r-cubed course")
# plot_quantitative_feedback

# This will be saved in the r-cubed folder, so needs to be pasted over.
ggsave(here::here("plot-feedback.pdf"), plot_quantitative_feedback,
       height = 4.5, width = 5.5)

# Word cloud?
# library(tidytext)
# here("feedback/2020-06-dda-survey-comments.csv") %>%
#     read_csv()

# Expectations and outcome plot?
#
